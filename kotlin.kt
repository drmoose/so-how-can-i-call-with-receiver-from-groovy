import groovy.lang.Closure

class KotlinReceiver { 
    fun hello() { 
        println("Hello from Kotlin") 
    } 
}

class KotlinVersion { 
    fun withReceiver(fn: KotlinReceiver.() -> Unit) {
         KotlinReceiver().fn()
    }

    // This overload is necessary for
    //    (new KotlinVersion()).withReceiver { hello () }
    // to work in Groovy. I would like to find a way to not have to write it
    // by hand for every function I define in kotlin.
    fun withReceiver(fn: Closure<Any>) = withReceiver {
        fn.delegate = this
        fn.resolveStrategy = Closure.DELEGATE_FIRST
        fn.run()
    }
}
