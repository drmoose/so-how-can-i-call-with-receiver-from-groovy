To compile:


```bash
# Adjust classpath to wherever groovy-2.5.0.jar is on your machine
kotlinc -cp /usr/share/groovy/lib/groovy-2.5.0.jar *.kt
groovyc groovy.groovy
```

To run:

```bash
# Adjust classpath to wherever kotlin-stdlib.jar is on your machine
groovy -cp /usr/share/kotlin/lib/kotlin-stdlib.jar groovy
```

Classpath arguments above are correct for Arch Linux, adjust for your OS as necessary.
